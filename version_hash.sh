#!/usr/bin/env bash
# This little script helps tracking the source hashes required for the Dockerfile

# postgis dependency
export GEOS_VERSION=3.9.1

# postgis dependency
export GDAL_VERSION=3.2.2

# postgis dependency
export PROJ4_VERSION=6.3.2

# postgis dependency
export PROTOBUFC_VERSION=1.3.3

export POSTGIS_VERSION=3.1.1

export PGROUTING_VERSION=3.1.3

export OSM2PGSQL_VERSION=1.4.2

wget -q -O geos.tar.gz "https://github.com/libgeos/geos/archive/$GEOS_VERSION.tar.gz"
echo "$GEOS_VERSION"
sha256sum geos.tar.gz

wget -q -O gdal.tar.gz "https://github.com/OSGeo/gdal/archive/v$GDAL_VERSION.tar.gz"
echo "$GDAL_VERSION"
sha256sum gdal.tar.gz

wget -q -O proj4.tar.gz "https://github.com/OSGeo/proj.4/archive/$PROJ4_VERSION.tar.gz"
echo "$PROJ4_VERSION"
sha256sum proj4.tar.gz

wget -q -O protobufc.tar.gz "https://github.com/protobuf-c/protobuf-c/archive/v$PROTOBUFC_VERSION.tar.gz"
echo "$PROTOBUFC_VERSION"
sha256sum protobufc.tar.gz

wget -q -O postgis.tar.gz "https://github.com/postgis/postgis/archive/$POSTGIS_VERSION.tar.gz"
echo "$POSTGIS_VERSION"
sha256sum postgis.tar.gz

wget -q -O pgrouting.tar.gz "https://github.com/pgRouting/pgrouting/archive/v$PGROUTING_VERSION.tar.gz"
echo "$PGROUTING_VERSION"
sha256sum pgrouting.tar.gz

wget -q -O osm2pgsql.tar.gz "https://github.com/openstreetmap/osm2pgsql/archive/$OSM2PGSQL_VERSION.tar.gz"
echo "$OSM2PGSQL_VERSION"
sha256sum osm2pgsql.tar.gz

rm -f *.tar.gz
